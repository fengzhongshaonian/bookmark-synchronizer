#!/bin/bash

project_home=$(cd ../ && pwd)
echo "project_home: $project_home"

function clean_old_dist() {
    cd "$project_home/build" && rm -rf ./dist
}

function build_module_core(){
  cd "$project_home/core" && yarn && tsc && mkdir -p "$project_home/build/dist/node" && cp -rf dist/. "$project_home/build/dist/node/" && cp -rf node_modules "$project_home/build/dist/node/"
}

function build_module_ui() {
    cd "$project_home" && cd ui && yarn && yarn run build && cp -rf dist "$project_home/build/"
}

function clean_map_js() {
    cd "$project_home/build/dist/js" && rm -f *.js.map && cd "$project_home/build/dist/node" && find ./ -name '*.js.map' | xargs rm -f
}

function copy_readme() {
    cd "$project_home" && cp -f README.md "$project_home/build/dist/"
}

clean_old_dist && build_module_ui && build_module_core && clean_map_js && copy_readme && echo '处理完成！'
