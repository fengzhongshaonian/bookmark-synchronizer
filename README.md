# Chrome浏览器书签同步工具



## 功能

实现Chrome浏览器书签同步功能（因为本插件是基于文件的方式进行同步的，所以使用本插件可以同步任何单个的文件，不仅仅是Chrome浏览器的书签）。

本插件基于Gitee仓库，远端的文件被存放在Gitee仓库中，所以您需要申请一个Gitee账号。



## 使用说明

### (1) 打开插件界面

安装完本插件之后，在utools中输入【书签同步】或者【同步书签】，即可进入本插件。

### (2) 配置插件

接着您需要填写一些配置信息：

- **书签存放路径：** 这个路径指的是你的Chrome浏览器书签文件的路径，查看该路径的办法：在Chrome浏览器中输入[chrome://version/](chrome://version/)，然后在打开的页面中可以找到“**Profile Path**”，书签文件就是存放在“**Profile Path**”所指定的目录中（假设这个目录是`/home/xxx/.config/google-chrome/Default`）。Chrome浏览器的书签文件名为`Bookmarks`，那么**书签存放路径**这一项可以填：`/home/xxx/.config/google-chrome/Default/Bookmarks`
- **用户授权码：** 打开[Gitee官网](https://gitee.com/)，登录，打开【个人设置】，找到【安全设置】下的【私人令牌】这一项，打开，接着生成私人令牌。**私人令牌**就是用户授权码。
- **仓库拥有者：** Gitee的个人空间地址。比如，我登录[Gitee官网](https://gitee.com/)之后，我的浏览器地址是`https://gitee.com/fengzhongshaonian`，那么仓库的拥有者就是`fengzhongshaonian`
- **仓库名称：** 自己建一个仓库用来存放你的书签文件，为了你的隐私安全，建议建一个**私有**的仓库。**创建仓库的时候记得同时初始化仓库，只有初始化之后的仓库才能被本插件所使用。**
- **仓库中书签文件路径：** 你打算将书签存放到仓库的哪个路径下，例如我想将书签存放到仓库根目录下的Bookmarks文件中，那么这一项的值就可以填`Bookmarks`，如果我想将书签存放到仓库根目录下的bookmarks目录下的Bookmarks文件中，那么这一项的值就可以填`bookmarks/Bookmarks`

### (3) 同步

插件的界面很简洁，相信聪慧如你一看就可以弄明白。这里简单地补充一下：

- 界面右上角的开个用来“开启/关闭书签自动同步服务”，这个开关如果打开的话，本插件会定时（每隔一分钟左右）检查一下是否需要同步，如果需要同步的话就会自动同步。
- 建右下角的同步按钮用来触发立即同步，这个功能即使不打开上面说的这个开关也可以使用，只要前面说的配置 填好了就行。
- 更新本地书签之后，可能需要重启一下浏览器才能看到同步后的结果。



## 界面截图

![](https://gitee.com/fengzhongshaonian/picture-bed/raw/master/picture/ui.png)



## 写这个插件的原因

由于在国内不FQ的话使用不了谷歌的服务，导致浏览器的书签没法同步。有一些第三方的浏览器插件倒是能提供书签同步的功能，但是安装这些插件通常也得FQ，很麻烦，所以写了这个插件。



## 开源

本插件已经在Gitee上开源，仓库的地址是：[https://gitee.com/fengzhongshaonian/bookmark-synchronizer](https://gitee.com/fengzhongshaonian/bookmark-synchronizer)

欢迎大家Star哦！



## 历史记录

- v0.0.2 修复**书签存放路径**重启后丢失的bug
- v0.0.1 实现基本功能