import ContentProvider from "../api/content-provider";
import ContentPropsProvider from "../api/content-props-provider";
import ContentUpdater from "../api/content-updater";

export default interface RemoteAccess extends ContentProvider, ContentPropsProvider, ContentUpdater{

    pushContent(content: ContentProvider): Promise<any>;

}
