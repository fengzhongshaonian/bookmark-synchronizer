
import {
    ConfigProvider,
    BookmarkManager,
    Decisions,
    DecisionMaker,
    FirstRunRecorderImpl
} from './index'

const localBookmarkPath = "";
const owner = '';
const repository = '';
const path = '';
const accessToken = '';

class Config implements ConfigProvider{

    private config: Map<string, any>;

    constructor() {
        this.config = new Map<string, any>();
        this.config.set('local.bookmark-path', localBookmarkPath);
        this.config.set('remote.gitee.owner', owner);
        this.config.set('remote.gitee.repository-name', repository);
        this.config.set('remote.gitee.bookmark-path', path);
        this.config.set('remote.gitee.access-token', accessToken);
    }

    get(key: string): any {
        return this.config.get(key);
    }

}


class DecisionMakerWithConsole implements DecisionMaker{

    makeDecision(): Promise<Decisions> {

        return new Promise((resolve)=>{
            process.stdin.setEncoding('utf8');

            const question = `
        请选择：\n
        1. 什么也不做
        2. 覆盖本地的书签
        3. 覆盖服务器上的书签
        你的决定：
        `
            const readline = require('readline');

            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });

            rl.question(question, function(answer) {
                const content = answer.toString().trim();
                rl.close();
                if(content === '2'){
                    resolve(Decisions.KEEP_REMOTE_VERSION);
                    return;
                }else if(content === '3'){
                    resolve(Decisions.KEEP_LOCAL_VERSION);
                    return;
                }
                else {
                    resolve(Decisions.DO_NOTHING);
                }

            });

        })

    }
}

const bookmarkManager = new BookmarkManager(new Config(), new DecisionMakerWithConsole(), new FirstRunRecorderImpl(localBookmarkPath));
bookmarkManager.syncBookmark().then(r => {
    console.log(r)
});
