import FirstRunRecorder from "../api/first-run-recorder";
import * as fs from "fs";
import assert = require("assert");


export default class FirstRunRecorderImpl implements FirstRunRecorder{

    constructor(public bookmarkPath:string) {
        assert(bookmarkPath && bookmarkPath.trim() !== '')
    }

    isFirstRun(): boolean {
        // 检查Bookmarks.synchronize.bak是否存在
        // 如果存在，则任务不是第一次运行本程序
        const path = this.bookmarkPath + '.synchronize.bak';
        try {
            const stat = fs.statSync(path);
            return !stat.isFile();
        }catch (err){
            return true;
        }
    }

    setFirstRun(firstRun: boolean) {
        // 什么也不做
    }

}
