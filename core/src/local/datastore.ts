
// 存储应用数据

const low = require('lowdb')
const LodashId = require('lodash-id')
const FileSync = require('lowdb/adapters/FileSync')
const path = require('path')
const fs = require('fs-extra')

// 获取应用的用户目录
const os = require('os')

function getUserConfigDir(){
  const platform = os.platform();
  switch (platform){
    case "linux":
      return path.join(os.homedir(), '.config')
    case "darwin":
      return path.join(os.homedir(), 'Library/Application Support')
    case "win32":
      return path.join(os.homedir(), 'AppData\\Roaming')
    default:
      return os.homedir()
  }
}
const STORE_PATH = path.join(getUserConfigDir(), 'bookmark-synchronizer')

// 如果路径不存在, 则创建
if (!fs.pathExistsSync(STORE_PATH)) {
  fs.mkdirpSync(STORE_PATH)
}

// 初始化lowdb读写的json文件名以及存储路径
const adapter = new FileSync(path.join(STORE_PATH, '/data.json'))

// lowdb接管该文件
const db = low(adapter)
db._.mixin(LodashId)

if (!db.has('config').value()) {
  db.set('config.local.bookmark-path', '').write()
}

module.exports = db
