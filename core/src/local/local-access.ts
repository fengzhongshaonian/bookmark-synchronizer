import ContentProvider from "../api/content-provider";
import ContentPropsProvider from "../api/content-props-provider";

export default interface LocalAccess  extends ContentProvider, ContentPropsProvider{

    update(content: Buffer): Promise<any>;

}