
export default interface ContentProvider{

    getContent(): Promise<Buffer>;

}
