

export default interface ConfigProvider{

    get(key: string): any;
}