
export default interface FirstRunRecorder {

    isFirstRun(): boolean;

    setFirstRun(firstRun: boolean);
}
