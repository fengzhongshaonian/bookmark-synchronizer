
export enum Decisions {
    DO_NOTHING = '什么也不做',
    KEEP_LOCAL_VERSION = '保留本地版本，用本地版本覆盖远程服务器上的版本',
    KEEP_REMOTE_VERSION= '放弃本地版本，用远程服务器的版本覆盖本地版本'
}
export interface DecisionMaker {

    makeDecision(): Promise<Decisions>;
}