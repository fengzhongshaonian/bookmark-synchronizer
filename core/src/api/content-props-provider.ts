
export default interface ContentPropsProvider{

    getLastModifiedTime(): Promise<Date>;

}