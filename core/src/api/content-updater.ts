
export default interface ContentUpdater {

    update(buffer: Buffer): Promise<any>;
}
