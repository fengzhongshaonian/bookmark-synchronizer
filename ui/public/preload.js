
const {
    BookmarkManager,
    Decisions,
    DecisionMaker,
    FirstRunRecorderImpl,
    db
} = require('./node/index')

const utools = window.utools;

class Config {
    get(key){
        const result = utools.db.get(key);
        return result? result.data : undefined;
    }
    set(key, value){
        let config = utools.db.get(key);
        if(config){
            config.data = value;
        }else {
            config = {
                _id: key,
                data: value
            }
        }
        utools.db.put(config)
    }
}
const config = new Config();

// 做一下拦截：对于以"local."开头的配置
// 不存储在utools的数据库中，改为本地存储
class ConfigProxy extends Config{
    get(key){

        if(key && key.startsWith('local.')){
            return db.read().get('config.' + key).value();
        }
        return config.get(key);
    }

    set(key, value){
        if(key && key.startsWith('local.')){
            db.read().set('config.' + key, value).write();
            return;
        }
        config.set(key, value);
    }
}

window.plugin_api = {
    BookmarkManager,
    Decisions,
    DecisionMaker,
    FirstRunRecorderImpl,
    config: new ConfigProxy(),
    pluginStatus: { isReady: false}
}

utools.onPluginReady(() => {
    window.plugin_api.pluginStatus.isReady = true;
})