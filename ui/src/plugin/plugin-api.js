
const SyncResult = {
  DO_NOTHING: '什么也没做',
  NO_NEED_TO_SYNC: '本地书签和远程书签一致，无需同步',
  LOCAL_BOOKMARKS_UPDATED: '本地书签被更新',
  REMOTE_BOOKMARKS_UPDATED: '远程书签被更新'
}

class FakeBookmarkManager{
  constructor(configProvider,  decisionMaker, firstRunRecorder) {
    this.configProvider = configProvider;
    this.decisionMaker = decisionMaker;
    this.firstRunRecorder = firstRunRecorder;
  }

  async syncBookmark(){
    return Promise.resolve(SyncResult.DO_NOTHING);
  }
}

const FakeDecisions = {
  DO_NOTHING: '什么也不做',
  KEEP_LOCAL_VERSION: '保留本地版本，用本地版本覆盖远程服务器上的版本',
  KEEP_REMOTE_VERSION: '放弃本地版本，用远程服务器的版本覆盖本地版本',
}

class FakeDecisionMaker{
  async makeDecision(){
    return Promise.resolve(FakeDecisions.DO_NOTHING);
  }
}

class FakeFirstRunRecorderImpl{
  isFirstRun() {
    return false;
  }

  setFirstRun(firstRun) {
    // 什么也不做
    console.log('你打算将firstRun设置为：', firstRun)
  }
}

const Keys = {
  LOCAL_BOOKMARK_PATH: 'local.bookmark-path',
  REMOTE_GITEE_OWNER: 'remote.gitee.owner',
  REMOTE_GITEE_REPOSITORY_NAME: 'remote.gitee.repository-name',
  REMOTE_GITEE_BOOKMARK_PATH: 'remote.gitee.bookmark-path',
  REMOTE_GITEE_ACCESS_TOKEN: 'remote.gitee.access-token'
}

class FakeConfig {

  constructor() {
    this.config = new Map();
        this.config.set('local.bookmark-path', '');
  }
  get(key){
    return this.config.get(key);
  }

  set(key, value){
    this.config.set(key, value);
  }

}

const plugin_api = window.plugin_api? window.plugin_api : {
  BookmarkManager: FakeBookmarkManager,
  Decisions: FakeDecisions,
  DecisionMaker: FakeDecisionMaker,
  FirstRunRecorderImpl: FakeFirstRunRecorderImpl,
  config: new FakeConfig(),
  pluginStatus: { isReady: false}
};

export default {
  ...plugin_api,
  Keys
}